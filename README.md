# DVRA - Damn Vulnerable Flask App

A Python3/Flask application vulnerable to OWASP Top 10.

### Installation

The easiest way to run DVFA is cloning the repository and use Docker.

```sh
$ cd DVFA/
$ docker image build -t dvfa:1.0 . 
$ docker container run --publish 5000:5000 --detach --name dvfa-container dvfa:1.0
```

Then just browse to http://localhost:5000/

![DVFA](DVFA.png "DVFA")
