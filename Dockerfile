FROM mysql:latest
RUN apt-get update
RUN echo y | apt-get install python3 python3-pip libxml2-dev libxslt1-dev python3-dev gcc libc6-dev zlib1g-dev
RUN pip3 install libxml2-python3 flask sqlalchemy ariadne lxml flask_sqlalchemy
RUN mysqld --initialize-insecure
WORKDIR /usr/src/app
COPY . .
RUN $(mysqld --user=root) & sleep 2; mysql < schema.sql
EXPOSE 5000
ENTRYPOINT ["flask", "run", "--host=0.0.0.0"]
